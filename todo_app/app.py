import json
from urllib.parse import parse_qs, urlparse
from flask import Flask, redirect, request, render_template
import jwt
import requests
import pymongo
from todo_app.data.User import User
from todo_app.flask_config import Config
from todo_app.data.items import add_card_to_a_list, update_item_status, get_cards_on_board
from todo_app.data.view_model import ViewModel
 # from pymongo import MongoClient
from flask_login import LoginManager, login_required, login_user, current_user

import certifi, os
 # Add stdout handler, with level INFO


def create_app():
    
    app = Flask(__name__)
    app.secret_key = os.environ.get('APP_SECRET_KEY')
    login_manager = LoginManager()


    @login_manager.unauthorized_handler
    def unauthenticated():
        url = 'https://gitlab.com/oauth/authorize?client_id='+os.environ.get('APP_ID')+'&response_type=code&scope=openid&redirect_uri='+os.environ.get('REDIRECT_URI')
        return redirect(url)

    @login_manager.user_loader
    def load_user(user_id):
        return User(user_id)

    login_manager.init_app (app)

    client = pymongo.MongoClient (os.getenv('MONGO_DB_DETAILS'),tlsCAFile=certifi.where())
    db = client['task_tracker_db']
    
    @app.route('/login/callback')
    def callback():
        param_list = {
            'client_id':os.getenv('APP_ID'),
            'client_secret': os.getenv('APP_SECRET'),
            'code':request.args.get('code'),
            'grant_type':'authorization_code',
            'redirect_uri':os.environ.get('REDIRECT_URI')
        }
        response = requests.post(
        'https://gitlab.com/oauth/token',
        params = param_list
        )
        # Returns in json (access_token, token+_type, expires_in, refresh_token,scope, created_at, id_token
        json_loads=json.loads(response.text)
        for key in json_loads:
            if (key) == "id_token":
                id_token_value = json_loads[key]

        jwt_dict = jwt.decode(id_token_value,options={"verify_signature": False})
        print (jwt_dict)
        for key in jwt_dict.keys():
            if key == "sub":
                sub_value = jwt_dict[key]   
        user = User(sub_value)
        login_user(user)
        return redirect('/')   

    @app.route('/')
    @login_required
    def index():
        items = get_cards_on_board (db)
        item_view_model = ViewModel(items)
        return render_template('index.html', view_model = item_view_model)  

    @app.route('/additem', methods=['POST']) 
    @login_required     
    def additem():
        if current_user.role == 'reader':
            return ('User has no permissions to Add Item.Please check with administrator.')
        else:
            input_item = request.form.get('item')
            status = request.form.get('status')
            add_card_to_a_list(input_item, status,db)
            return redirect('/')   

    @app.route('/updateItemStatus', methods=['POST'])      
    @login_required
    def update_status():
        if current_user.role == 'reader':
                return ('User has no permissions to Update Item. Please check with administrator.')
        else:
            input_item_id = request.form.get('id')
            updated_status = request.form.get('status')
            input_item_name = request.form.get('name')
            old_status = request.form.get('old_status')
            update_item_status(input_item_id, input_item_name, updated_status,old_status, db)
            return redirect('/')

    return app