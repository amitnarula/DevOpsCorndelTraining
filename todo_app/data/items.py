from todo_app.data.item import Item

def get_cards_on_board(db):
        """ Returns all current cards in the mongo database
        """
        collection_list = ['to_do','doing','done']
        item_list = []
        for collection in collection_list:
            for document in db[collection].find({}):
                card_id = document.get("_id")
                card_name = document.get("name")
                item_list.append (Item(card_id,card_name,collection))    
        return item_list

def update_item_status (item_id,item_name,item_status,old_status,db):
    collection = db[item_status]
    collection.insert_one({'name':item_name})
    collection = db[old_status]
    collection.delete_one({"_id":item_id})

def add_card_to_a_list(card_name, status, db):
    """ 
        Add card to collection in Mongo DB
    """
    collection = db[status]
    collection.insert_one({'name':card_name})

