 # Creating item class to map Trello API reponse with item attributes
class Item:
    def __init__(self, id, name, status = 'To Do'):
        self.id = id
        self.name = name
        self.status = status