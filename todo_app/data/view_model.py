from todo_app.data.item import Item

class ViewModel:
    def __init__(self, items):
            self._items = items

    @property
    def items(self):
        return self._items

    @property
    def doing_items(self):
        return [item for item in self._items if item.status == "doing"]

    @property
    def todo_items(self):
        return [item for item in self._items if item.status == "to_do"]

    @property
    def done_items(self):
        return [item for item in self._items if item.status == "done"]