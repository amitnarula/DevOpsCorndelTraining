FROM python:3.10-slim-buster as base
ENV POETRY_HOME=/etc/poetry
RUN apt-get update && apt-get install -y curl && curl -sSL https://install.python-poetry.org | python3 - --version 1.1.13
ENV PATH="$PATH:$POETRY_HOME/bin"
WORKDIR /opt/
COPY . /opt/
RUN poetry install
EXPOSE 8080

FROM base as production
CMD [ "poetry", "run" , "gunicorn" , "-b", "0.0.0.0:8080", "todo_app.app:create_app()"]

FROM base as development
CMD [ "poetry", "run" , "flask" , "run" , "-p 8080", "--host=0.0.0.0"]

FROM base as test

ENTRYPOINT [ "poetry", "run", "pytest" ]