# DevOps Apprenticeship: Project Exercise (Module 11)

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Module 11 : Add GitLab authentication and authorization to app
A) Register app on GitLab (with local host for development and azureapp for live app)
B) Install flask-login via poetry command
C) Setup user authentication via Callback URL logic and implement the Callback route
D) If user authentication via GitLab is successful, then check User authorization (reader/writer) and display functionalities
E) Update test to ignore authentication and authorization
More details at https://project-exercises.devops.corndel.com/exercises/m11_exercise_gitlab#part-3-adding-authorisation

## TDD (Test Driven Development) setup for this app & Running the test
Run "poetry add pytest --dev" to add pytest as dependency of our project.
Test can be executed either by running "pytest" or "poetry run pytest" or "poetry run pytest path/to/test_file"
Test in docker image can also be executed by running the below command :
"docker build --target test --tag TEST_IMAGE ."
"docker run --env-file .env.test TEST_IMAGE"

## Gitlab CI/CD process
A) Create Test Docker Image for app and push to Docker Hub (https://hub.docker.com/r/narulaam/todo_app_test_image/tags) via GitLab pipeline (part of previous module).
B) Create Azure Webapp via CLI or from Portal : https://todoappamit.azurewebsites.net/ (part of previous module). 
C) Updated Gitlab yaml file to push Prod Docker images (https://hub.docker.com/r/narulaam/todo_app_prod_image/tags) to Azure Webapp automatically via WebHook when build happens in master (part of this module).
D) Verify app works via https://todoappamit.azurewebsites.net/