import mongomock, pymongo, os, pytest
from dotenv import load_dotenv, find_dotenv
from todo_app.data.items import add_card_to_a_list
from todo_app import app

@pytest.fixture
def client():
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        test_app.config['LOGIN_DISABLED'] = True
        with test_app.test_client() as client:
            yield client
    
def test_index_page(client):
    db = pymongo.MongoClient(os.getenv('MONGO_DB_DETAILS'))['task_tracker_db']
    add_card_to_a_list('Test Card', 'to_do', db)
    response = client.get('/')

    assert response.status_code == 200
    assert 'Test Card' in response.data.decode()