import unittest
from todo_app.data.view_model import ViewModel
from todo_app.data.item import Item

class TestViewModel(unittest.TestCase):

    items = [ 
        Item(1,"London","to_do"),
        Item(2,"Manchester","doing"),
        Item(3,"Brighton","done"),
        Item(4,"Cardiff","to_do"),
        Item(5,"Oxford","doing")
    ]

    def test_doing_items(self):
        viewModel = ViewModel (TestViewModel.items)
        doing_items = viewModel.doing_items
        doing_items_ids = [item.id for item in doing_items]
        print (doing_items_ids)
        for item in doing_items:
            self.assertEqual(item.status,"doing","doing_items function implemented incorrectly")
        
        assert len(doing_items) == 2

        assert 1 not in doing_items_ids
        assert 2 in doing_items_ids
        assert 3 not in doing_items_ids
        assert 4 not in doing_items_ids
        assert 5 in doing_items_ids

    def test_todo_items(self):
        viewModel = ViewModel (TestViewModel.items)
        todo_items = viewModel.todo_items
        todo_items_ids = [item.id for item in todo_items]
        for item in todo_items:
            self.assertEqual(item.status,"to_do","todo_items function implemented incorrectly")
        
        assert len(todo_items) == 2

        assert 1 in todo_items_ids
        assert 2 not in todo_items_ids
        assert 3 not in todo_items_ids
        assert 4 in todo_items_ids
        assert 5 not in todo_items_ids 


    def test_done_items(self):
        viewModel = ViewModel (TestViewModel.items)
        done_items = viewModel.done_items
        done_items_ids = [item.id for item in done_items]
        for item in done_items:
            self.assertEqual(item.status,"done","done_items function implemented incorrectly")

        assert len(done_items) == 1

        assert 1 not in done_items_ids
        assert 2 not in done_items_ids
        assert 3 in done_items_ids
        assert 4 not in done_items_ids
        assert 5 not in done_items_ids
        

if __name__ == '__main__':
    unittest.main()